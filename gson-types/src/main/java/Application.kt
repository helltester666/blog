import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.GsonBuilder

fun main(arg: Array<String>) {
    val gson = GsonBuilder().create()
    val mapper = ObjectMapper()

    val json = "{\"someProperty\": 120}"

    val gsonObj = gson.fromJson(json, Any::class.java)
    val gsonStr = gson.toJson(gsonObj, Any::class.java)

    val jacksonObj = mapper.readValue(json, Any::class.java)
    val jacksonStr = mapper.writeValueAsString(jacksonObj)

    println(gsonObj)
    println(gsonStr)

    println(jacksonObj)
    println(jacksonStr)
}