package suites

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.fail
import org.junit.platform.engine.discovery.DiscoverySelectors
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder
import org.junit.platform.launcher.core.LauncherFactory
import org.junit.platform.launcher.listeners.SummaryGeneratingListener

class SuiteExtensionRunner : BeforeAllCallback {
    override fun beforeAll(context: ExtensionContext?) {
        val request = LauncherDiscoveryRequestBuilder.request()
            .selectors(
                DiscoverySelectors.selectPackage("copter")
            )
            .build()
        val launcher = LauncherFactory.create()
        val listener = SummaryGeneratingListener()
        launcher.registerTestExecutionListeners(listener)

        launcher.execute(request)

        if (listener.summary.testsFailedCount > 0) {
            fail("Некоторые тесты упали, см. отчеты Allure для подробностей")
        }
    }
}

@ExtendWith(SuiteExtensionRunner::class)
class Suite {
    @Test
    fun runSuite() {}
}