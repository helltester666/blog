package copter

import TestBase
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Screw1Test : TestBase() {

    @Test
    fun start1ScrewTest() {
        println("Check engine 1 started")
        assertTrue(copter.start1Screw())
    }
}