package copter

import TestBase
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Screw2Tests : TestBase() {

    @Test
    fun start2ScrewTest() {
        println("Check engine 2 started")
        assertTrue(copter.start2Screw())
    }
}