package copter

import TestBase
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Screw3Tests : TestBase() {

    @Test
    fun start3ScrewTest() {
        println("Check engine 3 started")
        assertTrue(copter.start3Screw())
    }
}