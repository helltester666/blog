package copter

import TestBase
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Screw4Tests : TestBase() {

    @Test
    fun start4ScrewTest() {
        println("Check engine 4 started")
        assertTrue(copter.start4Screw())
    }
}